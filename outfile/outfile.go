package outfile

import (
	"bufio"
	"database/sql"
	"io"
)

// StreamRows simulates the MySQL SELECT ... INTO OUTFILE statement,
// but using the standard wire protocol so that access to the server's
// local filesystem is not required.
//
// The OPTIONALLY modifier to FIELDS [OPTIONALLY] ENCLOSED BY is not
// supported, since it requires a full scan of the value being written.
func StreamRows(rows *sql.Rows, w io.Writer, p Parameters) error {

	columns, err := rows.ColumnTypes()
	if err != nil {
		return err
	}

	bs := byteset{}
	bs.Add(0)
	bs.Add(p.LinesTerminatedBy[0])
	bs.Add(p.FieldsEnclosedBy[0])
	bs.Add(p.FieldsEscapedBy[0])

	writers := make([]writerTo, len(columns))
	scanners := make([]interface{}, len(columns))
	for i, ct := range columns {
		//TODO: test by reflected type, or some other criterion?
		switch ct.DatabaseTypeName() {
		case "BIGINT", "INT", "TIMESTAMP":
			//TODO: maybe test if any parameters start with [-: ]
			writers[i] = &enclosedWriter{
				enclosedBy: p.FieldsEnclosedBy,
			}
		default:
			writers[i] = &escapedWriter{
				enclosedWriter: enclosedWriter{enclosedBy: p.FieldsEnclosedBy},
				escapedBy:      p.FieldsEscapedBy[0],
				bs:             bs,
			}
		}
		scanners[i] = writers[i].(buffered).Buffer()
	}

	bw := bufio.NewWriter(w)
	for rows.Next() {
		if err := rows.Scan(scanners...); err != nil {
			return err
		}
		bw.Write([]byte(p.LinesStartingBy))
		writers[0].WriteTo(bw)
		for _, s := range writers[1:] {
			bw.Write(p.FieldsTerminatedBy)
			s.WriteTo(bw)
		}
		bw.Write(p.LinesTerminatedBy)
	}
	bw.Flush()

	return nil

}
