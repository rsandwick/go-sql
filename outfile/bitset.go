package outfile

type byteset [8]uint32

//type byteset [4]uint64

func (bs *byteset) Add(c byte) {
	bs[c>>5] |= 1 << uint(c&31)
	//bs[c>>6] |= 1 << uint(c&63)
}

func (bs *byteset) Contains(c byte) bool {
	return (bs[c>>5] & (1 << uint(c&31))) != 0
	//return (bs[c>>6] & (1 << uint(c&63))) != 0
}

func (bs *byteset) IndexAny(s []byte) int {
	for i, c := range s {
		if bs.Contains(c) {
			return i
		}
	}
	return -1
}
