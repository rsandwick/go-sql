package outfile

import (
	"bufio"
	"database/sql"
)

type buffered interface {
	Buffer() *sql.RawBytes
}

type writerTo interface {
	WriteTo(*bufio.Writer) (int64, error)
}

type scanWriter struct {
	buf sql.RawBytes
}

func (s *scanWriter) Buffer() *sql.RawBytes {
	return &(s.buf)
}

// WriteTo writes buffered data to the given Writer. The raw byte
// buffer is written directly, which allows reuse of the sql.DB's
// buffers to avoid in-memory copying other than output buffering.
func (s *scanWriter) WriteTo(w *bufio.Writer) (int64, error) {
	n, err := w.Write(s.buf)
	return int64(n), err
}

type enclosedWriter struct {
	scanWriter
	enclosedBy []byte
}

// WriteTo writes buffered data to the given Writer. The raw byte
// buffer is written directly, which allows reuse of the sql.DB's
// buffers to avoid in-memory copying other than output buffering.
// The output is enclosed by the character supplied to the writerTo.
func (s *enclosedWriter) WriteTo(w *bufio.Writer) (int64, error) {
	var n, m int
	var err error
	if n, err = w.Write(s.enclosedBy); err == nil {
		if m, err = w.Write(s.buf); err == nil {
			n += m
			if m, err = w.Write(s.enclosedBy); err == nil {
				n += m
			}
		}
	}
	return int64(n), err
}

type escapedWriter struct {
	enclosedWriter
	escapedBy byte
	bs        byteset
}

// WriteTo writes buffered data to the given Writer. In this case,
// the transfer includes escaping according to MySQL rules for the
// SELECT ... INTO OUTFILE statements various parameters for writing
// fields and lines. For specifics, see:
//
//   https://dev.mysql.com/doc/refman/en/load-data.html
//
//TODO: finish implementing -- specifics may not be fully correct yet.
func (s *escapedWriter) WriteTo(w *bufio.Writer) (int64, error) {
	var n, m int
	var err error
	n, err = w.Write(s.enclosedBy)
	bs := s.bs
	i := 0
	j := bs.IndexAny(s.buf)
	for j >= 0 {
		if j > 0 {
			m, err = w.Write(s.buf[i : i+j])
			n += m
			if err != nil {
				return int64(n), err
			}
		}
		j += i
		c := s.buf[j]
		if c == 0 {
			c = '0'
		}
		if err = w.WriteByte(s.escapedBy); err != nil {
			return int64(n), err
		}
		n++
		if err = w.WriteByte(c); err != nil {
			return int64(n), err
		}
		n++
		i = j + 1
		j = bs.IndexAny(s.buf[i:])
	}
	m, err = w.Write(s.buf[i:])
	n += m
	if err != nil {
		return int64(n), err
	}
	m, err = w.Write(s.enclosedBy)
	n += m
	return int64(n), err
}
