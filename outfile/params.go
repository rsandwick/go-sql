package outfile

// Parameters describe the escaping and quoting options for an outfile.
//
// Quoting and escaping parameters must be single bytes, and the MySQL
// LINES STARTING BY option is not supported.
type Parameters struct {
	FieldsEnclosedBy,
	FieldsEscapedBy,
	FieldsTerminatedBy,
	LinesStartingBy,
	LinesTerminatedBy []byte
}

// DefaultParameters offers a reasonable baseline of outfile options.
var DefaultParameters = Parameters{
	FieldsEnclosedBy:   []byte{'"'},
	FieldsEscapedBy:    []byte{'\\'},
	FieldsTerminatedBy: []byte{','},
	LinesStartingBy:    []byte{},
	LinesTerminatedBy:  []byte{'\n'},
}
